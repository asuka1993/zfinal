"use strict"
var dbHelper = require('../common/dbHelper');
var user = {
    insert: function(_user, cb) {
        dbHelper.op(function(db) {
            var col = db.collection('user');
            col.insert(_user, function(err, result) {
                if (err) {
                    console.log("err in model user insert");
                    cb(err)
                }
                cb(result);
            });
        })
    },
    query: function(obj, cb) {
        dbHelper.op(function(db) {
            var col = db.collection('user');
            col.find(obj).toArray(function(err, items) {
                if (err) {
                    console.log("err in model user query 2 arr")
                    throw err;
                }
                cb(items);
            });
        });
    }
}
// user{
//     name,password 
//     nickname, intro,tags, 
// }

// {
//     result: {ok: 1,n: 1},
//     ops: [insertArr],
//     insertedCount: 1,
//     insertedIds: [567 acbc164f32c93e8c939c1]
// }
module.exports = user;
