;
(function() {
    "use strict"

    function toggle() {
        this.classList.toggle('active', !this.classList.contains('active'))
    }
    Array.prototype.forEach.call(document.querySelectorAll('[data-ctrl=dropdown]'), function(target) {
        //initApi
        target.toggle = toggle;
        target.addEventListener('click', target.toggle);
    });

    Array.prototype.forEach.call(document.querySelectorAll('[data-ctrl=count]'), function(target) {
        target.count = target.dataset['count'];
        target.querySelector('input[type=text], textarea').addEventListener('keyup', function() {
            var res = target.count - this.value.length;
            target.dataset['count'] = res <= 0 ?  0 :  res; 
         
        });
    });

    Array.prototype.forEach.call(document.querySelectorAll('[data-ctrl=dropbox]'), function(target) {

        target.addEventListener("dragenter", function(e) {
            this.classList.add("dragover");
        });
        target.addEventListener("dragleave", function(e) {
            this.classList.remove("dragover");
        });
        target.addEventListener("dragover", function(e) {
            e.preventDefault();
        });
    });

}());

;
(function() {
    "use strict"

    function toggle() {
        this.classList.toggle('active', !this.classList.contains('active'))
    }

    function liListen(e) {
        let index = Array.prototype.indexOf.call(this.parentElement.children, this);
        if (index != target.dataset['value']) {
            target.value(index, this.textContent)
        }
    }

    Array.prototype.forEach.call(document.querySelectorAll('[data-ctrl=select]'), function(target) {
        //init set
        let index = parseInt(target.dataset['value']);

        //initApi
        target.value = function(index, txt) {
            if (index === undefined) {
                return this.dataset['value'];
            } else {
                target.dataset['value'] = index;
                let input = target.firstElementChild;
                if (typeof input.value === "undefined") {
                    input = target.querySelector('input')
                }
                input.value = txt || target.querySelectorAll('ul.items li').item(index).textContent;
            }
        };
        //bind
        target.addEventListener('click', toggle);
        Array.prototype.forEach.call(target.querySelectorAll('ul.items li'), function(li) {
            li.addEventListener('click', liListen);
        });
    });


}())

;
(function() {
    "use strict"

    function toggle(active, next) {
        active.classList.remove('active');
        next.classList.add('active');
    }

    //static 
    let arr = {
        left: ['right', 'left'], //<-
        right: ['left', 'right'] //->
    }
    let TRANSTIME = 600;

    let proto = {
        cycleTime: 5000,
        sliding: false,
        isCycle: false,
        direction: 'right',
        to: function to(direction) {
            if (this.sliding) return;
            let cur = this.querySelector('ul.slider-list .active');
            let curI = Array.prototype.indexOf.call(cur.parentElement.children, cur);
            if (curI === direction) return;
            let curIndicator = this.indicators.children[curI]
            let next, indicator;

            if (typeof direction === "number") {
                let nextI = direction;
                direction = curI < nextI ? 'right' : 'left';
                next = cur.parentElement.children[nextI];
                indicator = this.indicators.children[nextI];
            } else {
                if (direction === 'left') {
                    next = cur.previousElementSibling || cur.parentElement.lastElementChild;
                    indicator = curIndicator.previousElementSibling || this.indicators.lastElementChild;
                } else {
                    next = cur.nextElementSibling || cur.parentElement.firstElementChild;
                    indicator = curIndicator.nextElementSibling || this.indicators.firstElementChild;

                }
            }

            next.classList.add(arr[direction][1]);
            next.offsetWidth;
            next.classList.add('active');
            next.classList.remove(arr[direction][1]);
            cur.classList.remove('active');
            cur.classList.add(arr[direction][0]);
            this.sliding = cur;
            curIndicator.classList.remove('active');
            indicator.classList.add('active');
        },

        cycle: function cycle(time, direction) {
            let target = this;
            target.cycleTime = time || target.cycleTime;
            target.direction = direction || target.direction;

            target.interval = setInterval(function() {
                if (this.sliding) return;
                target.to(target.direction);
            }, target.cycleTime);
        },

        pause: function pause(isPaused) {
            this.isCycle = !isPaused
            this.interval = clearInterval(this.interval);
        }
    }

    Array.prototype.forEach.call(document.querySelectorAll('[data-ctrl=slider]'), function(target) {

        //initApi
        Object.assign(target, proto);

        //bind
        target.isCycle = target.dataset['slider'] === 'on';
        target.isCycle && target.cycle();

        target.addEventListener('mouseover', target.pause);
        target.addEventListener('mouseout', function() {
            target.isCycle && target.cycle()
        });

        Array.prototype.forEach.call(target.querySelectorAll('ul.slider-list li'), function each(li) {
            li.addEventListener('transitionend', function handleTransEnd(e) {
                if (target.sliding === this) {
                    let cls = this.classList;
                    cls.remove('left');
                    cls.remove('right');
                    target.sliding = null;
                }
            });
        });

        Array.prototype.forEach.call(target.querySelectorAll('.ctrls i'), function(i) {
            let dir = i.classList.contains('left') ? 'left' : 'right';
            i.addEventListener('click', function (){
                target.to(dir);
            });
        });

        target.indicators = target.querySelector('.ctrls .indicators');
        target.indicators.addEventListener('click', function(e) {
            if (e.target === this) return;
            target.to(Array.prototype.indexOf.call(target.indicators.children, e.target));
        })

    })
}());

;
(function() {
    "use strict"
    let surfaceProto = {
        show: function() {
            this.style.display = "inline-block";
            this.offsetWidth;
            this.classList.add('active');
        },
        hide: function() {
            this.classList.remove('active');
        }
    }

    Array.prototype.forEach.call(document.querySelectorAll('[data-ctrl=surface], [data-ctrl=actionsheet], [data-ctrl=dialog]'), function(target) {
        //initApi
        Object.assign(target, surfaceProto);
       
        target.addEventListener('transitionend', function(e) {
            if (!this.classList.contains('active')) {
                this.style.display = "none";
            }
        });
        target.addEventListener('click', function(e) {
            if (e.target === this) {
                this.classList.remove('active');
            }
        });

        Array.prototype.forEach.call(target.querySelectorAll('[data-off]'), function(off) {
            off.addEventListener('click', function() {
                target.hide();
            })
        });
    });
}());

;(function(){
	Array.prototype.forEach.call(document.querySelectorAll('[data-on]'),function(trigger){

		trigger.addEventListener('click', function(){
			document.querySelector(trigger.dataset['on']).show();
		})
	})
}());
;
(function() {
    "use strict"
    let _ = {}
    _.forEach = function(arr, fn) {
        for (let i = 0; i < arr.length; i++) {
            fn(arr[i])
        }
    }
    _.serialize = function(form) {
        typeof form === "string" &&
            (form = document.querySelector(form))
        var result = [];
        // if(Object.getPrototypeOf(form) === HTMLForm)
        var arr = form.querySelectorAll('input, textarea');
        Array.prototype.forEach.call(arr, function(input) {
            result.push([input.name, input.value].join('='));
        })
        return result.join('&');
    }
    _.ajax = function(url, opt) {
        opt = opt || {};
        var req = new XMLHttpRequest();
        req.open(opt.method || 'get', url);
        //req.responseType = opt.responseType;
        req.setRequestHeader(
            "Content-Type", opt.contentType || "application/x-www-form-urlencoded");
        req.addEventListener('load', function() {

            let response = req.response;
            try {
                response = JSON.parse(req.response);
            } catch (e) {}
            Object.setPrototypeOf({
                status: req.status,
                statusText: req.statusText,
                responseText: req.responseText,
                responseXML: req.responseXML,
                getAllHeaders: req.getAllResponseHeaders.bind(req),
                getHeader: req.getResponseHeader.bind(req)
            }, response);
            opt.load && opt.load.call(null, response);
        });

        req.addEventListener('error', function() {
            console.log(req.status)
            console.log("error")
        });
        req.addEventListener('timeout', function() {
            console.log(req.status)
            console.log("timeout")
        });
        req.addEventListener('progress', function() {
            console.log(req.status)
            console.log("progress")
        });


        //req.addEventListener('progress',opt.progress)
        req.send(opt.data);
    }


    var toast = function toast(msg, opt) {
        let t = document.createElement('div');
        t.classList.add('toast');
        t.classList.add('ui');
        opt = opt || {};
        let html = opt.html || toast.opt.html;
        let trans = opt.trans || toast.opt.trans;
        let time = opt.time || toast.opt.time;
        let pos = opt.pos || toast.opt.pos;

        ((html === true) || (t.textContent = msg)) && (t.innerHTML = msg);
        if (pos) {
            pos = pos.split(" ");
            for (let i = 0; i < pos.length; i++) {
                t.classList.add(pos[i]);
            }
        } else {
            t.classList.add(pos[i]);
        }

        document.body.appendChild(t);
        if (trans === true) {
            t.classList.add('ui-trans');
            t.offsetWidth;
            t.classList.add('active');
        }

        setTimeout(function() {
            t.remove();
        }, time);
        return t;
    };
    toast.opt = {
        pos: "bottom",
        time: 2500,
        html: false,
        trans: true,
    };

    _.toast = toast;
    window._ = _;
}(window));
