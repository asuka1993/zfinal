var model = require('../model/user');
var bcrypt = require('bcrypt');

var user = {
    signin: function(_user, cb) {
        model.query({
            name: _user.name
        }, function(items) {
            if (!items[0]) {
                return cb();
            }
            bcrypt.compare(_user.password, items[0].password, function(err, res) {
                if (err) {
                    console.log("err in signin bcrypt");
                    throw err;
                }
                cb(res, items[0]);
            })
        });
    },

    signup: function(_user, cb) {
        user.checkName(_user, function(has) {
            if (has) {
                cb(false);
            } else {
                bcrypt.hash(_user.password, 10, function(err, hash) {
                    _user.password = hash;
                    model.insert(_user, function(result) {
                        var user = result.ops ? result.ops[0] : result;
                        // if result == error 
                        cb(result.ok === 1, user);
                    });
                });
            }
        })

    },
    checkName: function(_user, cb) {
        model.query({
            name: _user.name
        }, function(users) {
            cb(users.length > 0)
        });
    },
    profile: function(_user) {
        model.query(_user, function(items) {
            if (items[0]) return;
            cb(items[0]);
        })
    }
}
module.exports = user;

//
