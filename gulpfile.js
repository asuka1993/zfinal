'use strict'

var gulp = require('gulp');
var less = require('gulp-less');
var concat = require('gulp-concat');

gulp.task('js', function(){
	return gulp.src('src/js/*.js')
    .pipe(concat('util.js'))
    .pipe(gulp.dest('public/js'));
})
gulp.task('css', function () {
    gulp.src('src/less/*.less')
        .pipe(less())
        .pipe(concat('zfinal.css'))
        .pipe(gulp.dest('public/css'))
});

gulp.task('watch', function () {
    gulp.watch(['src/less/*.less', 'src/less/*/*.less'],['css']); //当所有less文件发生改变时，调用testLess任务
    gulp.watch(['src/js/*.js'],['js'])
});