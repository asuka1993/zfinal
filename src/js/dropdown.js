;
(function() {
    "use strict"

    function toggle() {
        this.classList.toggle('active', !this.classList.contains('active'))
    }
    Array.prototype.forEach.call(document.querySelectorAll('[data-ctrl=dropdown]'), function(target) {
        //initApi
        target.toggle = toggle;
        target.addEventListener('click', target.toggle);
    });

    Array.prototype.forEach.call(document.querySelectorAll('[data-ctrl=count]'), function(target) {
        target.count = target.dataset['count'];
        target.querySelector('input[type=text], textarea').addEventListener('keyup', function() {
            var res = target.count - this.value.length;
            target.dataset['count'] = res <= 0 ?  0 :  res; 
         
        });
    });

    Array.prototype.forEach.call(document.querySelectorAll('[data-ctrl=dropbox]'), function(target) {

        target.addEventListener("dragenter", function(e) {
            this.classList.add("dragover");
        });
        target.addEventListener("dragleave", function(e) {
            this.classList.remove("dragover");
        });
        target.addEventListener("dragover", function(e) {
            e.preventDefault();
        });
    });

}());
