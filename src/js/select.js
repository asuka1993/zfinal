;
(function() {
    "use strict"

    function toggle() {
        this.classList.toggle('active', !this.classList.contains('active'))
    }

    function liListen(e) {
        let index = Array.prototype.indexOf.call(this.parentElement.children, this);
        if (index != target.dataset['value']) {
            target.value(index, this.textContent)
        }
    }

    Array.prototype.forEach.call(document.querySelectorAll('[data-ctrl=select]'), function(target) {
        //init set
        let index = parseInt(target.dataset['value']);

        //initApi
        target.value = function(index, txt) {
            if (index === undefined) {
                return this.dataset['value'];
            } else {
                target.dataset['value'] = index;
                let input = target.firstElementChild;
                if (typeof input.value === "undefined") {
                    input = target.querySelector('input')
                }
                input.value = txt || target.querySelectorAll('ul.items li').item(index).textContent;
            }
        };
        //bind
        target.addEventListener('click', toggle);
        Array.prototype.forEach.call(target.querySelectorAll('ul.items li'), function(li) {
            li.addEventListener('click', liListen);
        });
    });


}())
