;
(function() {
    "use strict"
    let surfaceProto = {
        show: function() {
            this.style.display = "inline-block";
            this.offsetWidth;
            this.classList.add('active');
        },
        hide: function() {
            this.classList.remove('active');
        }
    }

    Array.prototype.forEach.call(document.querySelectorAll('[data-ctrl=surface], [data-ctrl=actionsheet], [data-ctrl=dialog]'), function(target) {
        //initApi
        Object.assign(target, surfaceProto);
       
        target.addEventListener('transitionend', function(e) {
            if (!this.classList.contains('active')) {
                this.style.display = "none";
            }
        });
        target.addEventListener('click', function(e) {
            if (e.target === this) {
                this.classList.remove('active');
            }
        });

        Array.prototype.forEach.call(target.querySelectorAll('[data-off]'), function(off) {
            off.addEventListener('click', function() {
                target.hide();
            })
        });
    });
}());
