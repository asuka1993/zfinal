;
(function() {
    "use strict"

    function toggle(active, next) {
        active.classList.remove('active');
        next.classList.add('active');
    }

    //static 
    let arr = {
        left: ['right', 'left'], //<-
        right: ['left', 'right'] //->
    }
    let TRANSTIME = 600;

    let proto = {
        cycleTime: 5000,
        sliding: false,
        isCycle: false,
        direction: 'right',
        to: function to(direction) {
            if (this.sliding) return;
            let cur = this.querySelector('ul.slider-list .active');
            let curI = Array.prototype.indexOf.call(cur.parentElement.children, cur);
            if (curI === direction) return;
            let curIndicator = this.indicators.children[curI]
            let next, indicator;

            if (typeof direction === "number") {
                let nextI = direction;
                direction = curI < nextI ? 'right' : 'left';
                next = cur.parentElement.children[nextI];
                indicator = this.indicators.children[nextI];
            } else {
                if (direction === 'left') {
                    next = cur.previousElementSibling || cur.parentElement.lastElementChild;
                    indicator = curIndicator.previousElementSibling || this.indicators.lastElementChild;
                } else {
                    next = cur.nextElementSibling || cur.parentElement.firstElementChild;
                    indicator = curIndicator.nextElementSibling || this.indicators.firstElementChild;

                }
            }

            next.classList.add(arr[direction][1]);
            next.offsetWidth;
            next.classList.add('active');
            next.classList.remove(arr[direction][1]);
            cur.classList.remove('active');
            cur.classList.add(arr[direction][0]);
            this.sliding = cur;
            curIndicator.classList.remove('active');
            indicator.classList.add('active');
        },

        cycle: function cycle(time, direction) {
            let target = this;
            target.cycleTime = time || target.cycleTime;
            target.direction = direction || target.direction;

            target.interval = setInterval(function() {
                if (this.sliding) return;
                target.to(target.direction);
            }, target.cycleTime);
        },

        pause: function pause(isPaused) {
            this.isCycle = !isPaused
            this.interval = clearInterval(this.interval);
        }
    }

    Array.prototype.forEach.call(document.querySelectorAll('[data-ctrl=slider]'), function(target) {

        //initApi
        Object.assign(target, proto);

        //bind
        target.isCycle = target.dataset['slider'] === 'on';
        target.isCycle && target.cycle();

        target.addEventListener('mouseover', target.pause);
        target.addEventListener('mouseout', function() {
            target.isCycle && target.cycle()
        });

        Array.prototype.forEach.call(target.querySelectorAll('ul.slider-list li'), function each(li) {
            li.addEventListener('transitionend', function handleTransEnd(e) {
                if (target.sliding === this) {
                    let cls = this.classList;
                    cls.remove('left');
                    cls.remove('right');
                    target.sliding = null;
                }
            });
        });

        Array.prototype.forEach.call(target.querySelectorAll('.ctrls i'), function(i) {
            let dir = i.classList.contains('left') ? 'left' : 'right';
            i.addEventListener('click', function (){
                target.to(dir);
            });
        });

        target.indicators = target.querySelector('.ctrls .indicators');
        target.indicators.addEventListener('click', function(e) {
            if (e.target === this) return;
            target.to(Array.prototype.indexOf.call(target.indicators.children, e.target));
        })

    })
}());
