;
(function() {
    "use strict"
    let _ = {}
    _.forEach = function(arr, fn) {
        for (let i = 0; i < arr.length; i++) {
            fn(arr[i])
        }
    }
    _.serialize = function(form) {
        typeof form === "string" &&
            (form = document.querySelector(form))
        var result = [];
        // if(Object.getPrototypeOf(form) === HTMLForm)
        var arr = form.querySelectorAll('input, textarea');
        Array.prototype.forEach.call(arr, function(input) {
            result.push([input.name, input.value].join('='));
        })
        return result.join('&');
    }
    _.ajax = function(url, opt) {
        opt = opt || {};
        var req = new XMLHttpRequest();
        req.open(opt.method || 'get', url);
        //req.responseType = opt.responseType;
        req.setRequestHeader(
            "Content-Type", opt.contentType || "application/x-www-form-urlencoded");
        req.addEventListener('load', function() {

            let response = req.response;
            try {
                response = JSON.parse(req.response);
            } catch (e) {}
            Object.setPrototypeOf({
                status: req.status,
                statusText: req.statusText,
                responseText: req.responseText,
                responseXML: req.responseXML,
                getAllHeaders: req.getAllResponseHeaders.bind(req),
                getHeader: req.getResponseHeader.bind(req)
            }, response);
            opt.load && opt.load.call(null, response);
        });

        req.addEventListener('error', function() {
            console.log(req.status)
            console.log("error")
        });
        req.addEventListener('timeout', function() {
            console.log(req.status)
            console.log("timeout")
        });
        req.addEventListener('progress', function() {
            console.log(req.status)
            console.log("progress")
        });


        //req.addEventListener('progress',opt.progress)
        req.send(opt.data);
    }


    var toast = function toast(msg, opt) {
        let t = document.createElement('div');
        t.classList.add('toast');
        t.classList.add('ui');
        opt = opt || {};
        let html = opt.html || toast.opt.html;
        let trans = opt.trans || toast.opt.trans;
        let time = opt.time || toast.opt.time;
        let pos = opt.pos || toast.opt.pos;

        ((html === true) || (t.textContent = msg)) && (t.innerHTML = msg);
        if (pos) {
            pos = pos.split(" ");
            for (let i = 0; i < pos.length; i++) {
                t.classList.add(pos[i]);
            }
        } else {
            t.classList.add(pos[i]);
        }

        document.body.appendChild(t);
        if (trans === true) {
            t.classList.add('ui-trans');
            t.offsetWidth;
            t.classList.add('active');
        }

        setTimeout(function() {
            t.remove();
        }, time);
        return t;
    };
    toast.opt = {
        pos: "bottom",
        time: 2500,
        html: false,
        trans: true,
    };

    _.toast = toast;
    window._ = _;
}(window));
