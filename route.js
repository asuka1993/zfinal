"use strict"
var router = require("express").Router();
var bodyParser = require('body-parser');
var expressSession = require('express-session')
var connectRedis = require('connect-redis');

var config = require('./config');
var redisStore = connectRedis(expressSession);


var session = expressSession({
    secret: config.cookieSecret,
    resave: true,
    saveUninitialized: false,
    cookie: {
        secure: true,
        maxAge: 60 * 60
    },
    store: new redisStore({
        url: config.cache
    }),
});

var txtParser = bodyParser.urlencoded({
    extended: false
});
// var a = require("express").Router();
// var b = require("express").Router();
// a.get("/", require("./controller/index"));
// var jsonParser = bodyParser.json();
// b.post('/user/login', txtParser, require('./controller/user').login);
//试一下几个router有区别吗
// router.use(a);
// router.use(b);

router.get('/',require('./controller/index'));
router.get('/graph', require('./controller/graph'));
router.post('/user/signup', session, txtParser, require('./controller/user').signup);
router.post('/user/signin', session, txtParser, require('./controller/user').signin);
router.get('/user/profile', session, require('./controller/user').profile);
// a.get('/article',require('个性化ornot'))
// a.get('/hot',require('个性化'))
// a.get('/chat',require('个性化'))
// a.get('/write',require('些文章'))
// a.post('/write/publish',require('发布'))
// a.get('/:user/article',require('查看某人的文章'))

module.exports = router;
