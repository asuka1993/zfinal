var crypto = require('crypto');

var session = require('express-session')({
    resave: true,
    saveUninitialized: false,
    secret: config.sessionSecret,
    cookie: {
       
    },
    store: new RedisStore({
        url: config.cache
    })
});

var md5 = function(str) {
    var sum = crypto.createHash('md5');
    sum.update(str);
    return sum.digest('hex');
}

var encrypt = function(str, secret) {
    var cipher = crypto.createCipher('aes192', secret);
    var enc = cipher.update(str, 'utf8', 'hex');
    enc += cipher.final('hex');
    return enc;
}

var decrypt = function(str, secret) {
    var decipher = crypto.createDecipher('aes192', secret);
    var dec = decipher.update(str, 'hex', 'utf8');
    dec += decipher.final('utf8');
    return dec;
}

function authorize(req, res) {
    if (req.headers['Authorization']) {

    }    
}
