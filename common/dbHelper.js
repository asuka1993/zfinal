var MongoClient = require('mongodb').MongoClient;
var config = require("../config");

var url = config.db;
var dbHelper = {
    op : function (fn) {
        MongoClient.connect(url, function(err, db) {
        	if(err) throw err;
        	fn(db);
        });
    }
}
module.exports = dbHelper; 