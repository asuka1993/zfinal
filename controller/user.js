"use strict"
var proxy = require('../proxy/user');

function beforeSign(req, res) {
    if (req.body.name == "") {
        res.send({
            msg: '用户名不能为空'
        });
        res.status = 406;
        return;
    } else if (req.body.password === "") {
        res.send({
            msg: '密码不能为空'
        });
        res.status = 406;
        return;
    }
    return req.body;
}
var user = {
    signin: function(req, res) {
        var _user = beforeSign(req, res);
        if (!_user) return;
        proxy.signin(_user, function(isUser, u) {
            if (isUser) {
                res.send({
                    msg: '登陆成功'
                });
                req.session = u;
                res.status = 200
            } else {
                res.send({
                    msg: '用户名或密码名错误'
                });
                res.status = 401;
            }
        });
    },
    signup: function(req, res) {
        var _user = beforeSign(req, res);
        if (!_user) return;
        proxy.signup(_user, function(result, u) {
            if (result) {
                req.session.user = u;
                res.status = 201;
                res.end({
                    msg: '注册成功'
                });
                //先end 还是先status
            } else {
                //result : mongodb err name repeat
                res.status = 409;//account exist
                res.end({
                    msg : '用户名已存在'
                })
            }

        })
    },
    checkname : function (req, res){
        let _user = { name: req.query.name}
        proxy.checkName(_user, function (has){
            if(has){
                res.status = 409;
                res.end({
                    msg : '用户名已存在'
                })
            }else{
                res.status = 200 ; 
                res.end({
                    msg : '用户名可用'
                })
            }
        })
    },
    profile: function(req, res) {
        let _user = req.session.user;
        proxy.profile(_user, function(user) {
            res.render('profile', {
                user: user
            });
        });
    },
    logout : function (req, res){

    }
}


// var user = function(req, res) {
// }
module.exports = user;


//单元测试
//log
//debug
